import axios from 'axios'

export const api = axios.create({
    baseURL: 'https://lobinhos.herokuapp.com/wolves/'
})