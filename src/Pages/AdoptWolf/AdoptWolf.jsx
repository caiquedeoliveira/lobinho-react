import classes from './AdoptWolf.module.css'

export function AdoptWolf(){
    return(
        <>
            <main>
        <section className={classes.adoption}>
            <div className={classes.wolfAdoption}>
            <div clasNames={classes.wolfForAdoption}>
        <div className={classes.wolfSpecific}>
            <div className={classes.roundImage}>
                <div className={classes.wolfImage} ></div>
            </div>

            <div className={classes.wolfTitle}>
                <h1>Adote o(a) </h1>
                <p>ID:</p>
            </div>
        </div>
        </div>
            </div>


            <div className={classes.formAdoption}>
                <form>
                    <div className={classes.basicInfo}>
                        <div className={classes.name}>
                            <label for="name">Seu nome:</label>
                            <input type="text" id="adopter-name"/>
                        </div>
                        <div className={classes.age}>
                            <label for="age">Idade:</label>
                            <input type="text" id="adopter-age"/>
                        </div>
                    </div>

                    <div className={classes.addInfo}>
                        <div className={classes.email}>
                            <label for="email">E-mail:</label>
                            <input type="text" id="adopter-email"/>
                        </div>
                    </div>
                    
                </form>
            </div>

            
            <div className={classes.adoptButton}>
            </div>
        </section>
    </main>


        </>
 
    )
}

