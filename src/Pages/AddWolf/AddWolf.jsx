import { useState, useEffect } from 'react'
import { api } from '../../Services/api'
import classes from './AddWolf.module.css'

export function AddWolf(){

    const [name, setName] = useState('')
    const [age, setAge] = useState('')
    const [image, setImage] = useState('')
    const [description, setDescription] = useState('')
    const [wolves, setWolves] = useState([])

    function handleForm(e){
        e.preventDefault()
        let body = {
            name: name,
            age: age,
            image_url: image,
            description: description
        }

        api.post('/', body)
        .then(res => setWolves([...wolves, res.data]))
        .catch( err => console.log(err))
    }

    

    useEffect(() => {
        getWolves()
    }, [])

    function getWolves(){
        api.get('/')
        .then(res => setWolves(res.data))
        .catch(err => console.log(err))
    }

    return(
        <>
        <main>
        <section className={classes.add}>
            
            <h1>Coloque um Lobinho para Adoção</h1>

            <div className={classes.formAdd}>
                <form>
                    <div className={classes.basicInfo}>
                        <div className={classes.name}>
                            <label htmlFor="name">Nome do Lobinho:</label>
                            <input type="text" id="name" onChange={(e) => {setName(e.target.value)}}/>
                        </div>
                        <div className={classes.age}>
                            <label htmlFor="age">Anos:</label>
                            <input type="text" id="age" onChange={(e) => {setAge(e.target.value)}}/>
                        </div>
                    </div>

                    <div className={classes.addInfo}>
                        <div className={classes.linkImage}>
                            <label htmlFor="link_image">Link da Foto:</label>
                            <input type="text" name="" id="image" onChange={(e) => {setImage(e.target.value)}}/>
                        </div>
                    </div>

                    <div className={classes.addDescription}>
                        <div className={classes.description}>
                            <label htmlFor="description">Descrição</label>
                            <textarea id="description" onChange={(e) => {setDescription(e.target.value)}}></textarea>
                        </div>
                    </div>    
                </form>
            </div>

            
            <div className={classes.addButton}>
                <button onClick={handleForm}>Salvar</button>
            </div>
        </section>
    </main>

        </>
    )
}