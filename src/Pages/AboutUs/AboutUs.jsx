import classes from './AboutUs.module.css'

export function AboutUs(){
    return(
        <>
        <main>
        <section className={classes.aboutUs}>
            <h1>Quem somos</h1>
                <p>
                    Gingerbread cookie gingerbread jujubes cake biscuit tart fruitcake. Jujubes chupa chups wafer cheesecake lemon drops. Pastry caramels cookie marshmallow. Chupa chups jelly beans apple pie powder marzipan oat cake jelly-o chupa chups biscuit. Lollipop biscuit jujubes tootsie roll oat cake chocolate bar. 
                </p>
        </section>
        </main>

        </>
    )
}