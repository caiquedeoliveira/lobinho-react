import { useEffect, useState } from 'react'
import {api } from '../../Services/api'
import {Slogan} from '../../Components/Slogan/Slogan'
import {Values} from '../../Components/Values/Values'
import { WolfItem } from '../../Components/WolfItem/WolfItem'


export function Home(){
    const [wolves, setWolves] = useState([])

    useEffect(() => {
        getWolves()
    }, [])

    function getWolves(){
        api.get('/')
        .then(res => setWolves(res.data))
        .catch(err => console.log(err))
    }

    const twoWolves = wolves.slice(0, 2)

    return(
        <>
            <Slogan/>
            <Values/>
            {twoWolves.map(wolf => 
            <WolfItem
            image_url={wolf.image_url}
            name={wolf.name}
            age={wolf.age}
            description={wolf.description}
            >
            </WolfItem>
        )}
        </>
    )
}