import { useEffect, useState } from 'react'
import {api} from '../../Services/api.jsx'
import {WolfItem} from '../../Components/WolfItem/WolfItem'
import { FilterAdd } from '../../Components/FilterAdd/FilterAdd.jsx'
import classes from './AllWolves.module.css'


export function AllWolves(){
    const [wolves, setWolves] = useState([])

    useEffect(() => {
        getWolves()
    }, [])

    function getWolves(){
        api.get('/')
        .then(res => setWolves(res.data))
        .catch(err => console.log(err))
    }
    

    return(
        <>
        <main className={classes.main}>
            <FilterAdd/>
            {wolves.map(wolf => 
                <WolfItem
                image_url={wolf.image_url}
                name={wolf.name}
                age={wolf.age}
                description={wolf.description}
                id={Number(wolf.id)}
                key={Number(wolf.id)}
                >
                </WolfItem>
            )}
        </main>
        </>
    )
}