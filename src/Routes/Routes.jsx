import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'

import { Header } from '../Components/Header/Header'
import { Footer } from  '../Components/Footer/Footer'

import { Home} from '../Pages/Home/Home'
import { AboutUs } from '../Pages/AboutUs/AboutUs'
import { AddWolf } from '../Pages/AddWolf/AddWolf'
import { AllWolves } from '../Pages/AllWolves/AllWolves'
import { Error404 } from '../Pages/Error404/Error404'

import { AdoptWolf } from '../Pages/AdoptWolf/AdoptWolf.jsx'
import { SingleWolf } from '../Components/SingleWolf/SingleWolf'

export function AppRoutes(){
    return(
        <Router>
            <Header/>
            <Routes>
                <Route path={"/"} element={<Home/>}/>
                <Route path={"/quem-somos"} element={<AboutUs/>}/>
                <Route path={"/adicionar"} element={<AddWolf/>}/>
                <Route path={"/lobos"} element={<AllWolves/>}/>
                <Route path={"/lobo/:wolfId"} element={<SingleWolf/>}/>
                <Route path={"/lobo/:wolfId/adopt"} element={<AdoptWolf/>}/>
                <Route path={"*"} element={<Error404/>}/>
            </Routes>
            <Footer/>
        </Router>
    )
}