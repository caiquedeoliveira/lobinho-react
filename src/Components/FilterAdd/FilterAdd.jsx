import classes from './FilterAdd.module.css'
import { Link } from 'react-router-dom'

export function FilterAdd(){
    return(
        <>
        <div className={classes.filtering}>
            <section className={classes.searchWolf}>
                <input type="text" placeholder="Busque um Lobinho pelo nome"/>
                <Link to="/adicionar">+Lobo</Link>
            </section>
        
            <section className={classes.checkbox}>
                <input type="checkbox" className={classes.filter}/>
                <p>Ver lobinhos adotados</p>
            </section>
        </div>
        </>
    )

}