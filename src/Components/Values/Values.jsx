import classes from '../Values/Values.module.css'
import insurance from '../../imgs/Insurance.svg'
import group from '../../imgs/Group.svg'
import rescue from '../../imgs/rescue-dog.svg'
import care from '../../imgs/care.svg'

export function Values(){
    return(
        <>
            <section className={classes.values}>
            <h2>Valores</h2>
            <ul className={classes.valuesList}>
                <li>
                    <div className={classes.whiteCircle}>
                        <img src={insurance} alt="Proteção"/>
                    </div>
                    <div>
                        <h3>Proteção</h3>
                        <p>Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.</p>
                    </div>
                </li>

                <li>
                    <div className={classes.whiteCircle}>
                        <img src={care} alt="Carinho"/>
                    </div>
                    <div>
                        <h3>Carinho</h3>
                        <p>Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.</p>
                    </div>
                </li>

                <li>
                    <div className={classes.whiteCircle}>
                        <img src={group} alt="Companheirismo"/>
                    </div>
                    <div>
                        <h3>Companheirismo</h3>
                        <p>Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.</p>
                    </div>
                </li>

                <li>
                    <div className={classes.whiteCircle}>
                        <img src={rescue} alt="Resgate"/>
                    </div>
                    <div>
                        <h3>Resgate</h3>
                        <p>Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.</p>
                    </div>
                </li>
            </ul>
        </section>

        </>
    )
}