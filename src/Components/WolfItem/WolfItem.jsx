import classes from './WolfItem.module.css'
import { Link } from 'react-router-dom';

export function WolfItem(props){

    return(
        <>
            <section className={classes.wolvesList}>
            <li>
            <div className={classes.wolfImage} style={{backgroundImage: `url(${props.image_url})`}}></div>
                <div className={classes.wolfText}>
                    <div className={classes.wolfInfo}>
                        <div className={classes.nameAge}>
                            <h3 className={classes.wolfName}>{props.name}</h3>
                            <p className={classes.wolfAge}>Idade: {props.age} anos</p>
                        </div>
                        <div className={classes.adoptButton}>
                            {window.location.href !== "http://localhost:3000/" && <Link to={`/lobo/${props.id}`}>Adotar</Link>}
                        </div>
                    </div>
                    <p className={classes.wolfDescription}>{props.description}</p> 
            </div>
            </li>
        </section>
        </>
    )
}