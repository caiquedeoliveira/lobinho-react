import classes from './Header.module.css'
import logo from '../../imgs/Logo.svg'
import { Link } from 'react-router-dom'

export function Header(){
    return(
        <>
        <header className={classes.header}>
            <nav className={classes.navbarHeader}>
                <ul className={classes.navbarList}>
                    <li className={classes.animationHeader}>
                        <Link to="lobos">Nossos Lobinhos</Link>
                    </li>
                    <li>
                        <Link to="/">
                        <img src={logo} alt="Logo do Adote Lobinho"/>
                        </Link>
                    </li>
                    <li className={classes.animationHeader}>
                        <Link to="quem-somos">Quem somos</Link>
                    </li>
                </ul>
            </nav>
        </header>
        </>
    )
}