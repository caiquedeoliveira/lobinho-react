import classes from './SingleWolf.module.css'
import { useParams, Link } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { api } from '../../Services/api';



export function SingleWolf(){


    const { wolfId } = useParams();
    const [wolf, setWolf] = useState([])

    function getWolf(){
        api.get('/' + wolfId)
        .then(res => setWolf(res.data))
        .catch(err => console.log(err))
    }

    useEffect(() => {
        getWolf()
    }, [])


    return(
        <>
         <main>
             <section>
             <div className={classes.singleWolf}>
            <h1 className={classes.wolfTitle}>{wolf.name}</h1>
            <div className={classes.wolfInfo}>
                <div className={classes.imageButtons}>
                    <div>
                        <figure style={{backgroundImage: `url(${wolf.image_url})`}}></figure>
                    </div>
                    <div className={classes.wolfButtons}>
                        <Link to="">ADOTAR</Link>
                        <button type="button" className={classes.delete}>EXCLUIR</button>
                    </div>
                </div>
                <div className={classes.twop}>
                    <p className={classes.paragrafo}>{wolf.description}</p>
                    <p className={classes.paragrafo}>{wolf.description}</p>
                </div>
            </div> 
            </div>

             </section>
         </main>
        </>
    )
}